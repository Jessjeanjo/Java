package studentheightaverage;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		int studentNum;
		double maxHeight, heightAverage;
		double [] height;
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("How many students do you have?");
		studentNum = in.nextInt();
		
		height = new double[studentNum];
		
		System.out.println("Enter the height of student number 1 in inches:");
		height[0] = in.nextDouble();
		maxHeight = height[0];
		double heightTot = height[0];
		
		for (int i = 1; i < studentNum; i++){
			System.out.println("Enter the height of student number " +(i+1)+" in inches:");
			height[i] = in.nextDouble();
			
			if (height[i] > maxHeight){
				maxHeight = height[i];
			}
			heightTot += height[i];
		}
		
		heightAverage = heightTot/studentNum;
		
		System.out.println("You have a total of "+ studentNum+ " students in your class.\n"+
				"The maximum height in your class is "+maxHeight+".\n"+
				"The average height in your class is "+heightAverage+".");
		
		in.close();
		
	}
	

}
