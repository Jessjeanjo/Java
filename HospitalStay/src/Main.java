
package hospitalstay;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		boolean determinate = true;
		double overnightFee = 0, medFee, labServiceFee, feeTotal;
		int nightStay, anotherP;
		
		Scanner input = new Scanner(System.in);
		
		do{
			System.out.println("\nDid the patient stay overnight? Type 1 for yes or 0 for no.");
			nightStay = input.nextInt();
			
			System.out.println("What are the fees for the medical checkup?");
			medFee = input.nextDouble();
			
			System.out.println("What are the fees for the lab services?");
			labServiceFee = input.nextDouble();
			
			if (nightStay != 0){
				System.out.println("What are the fees for an overnight stay?");
				overnightFee = input.nextDouble();
				feeTotal = labServiceFee + medFee + overnightFee ;
			}
			
			feeTotal = labServiceFee + medFee ;
			
			System.out.print("\n\nYour medical fees are: $");
			System.out.println(medFee);
			System.out.print("Your lab service fees are: $");
			System.out.println(labServiceFee);
			if (nightStay != 0){
				System.out.print("Your overnight fees are: $");
				System.out.println(overnightFee);
			}
			
			System.out.print("\tYour TOTAL bill comes out to: $");
			System.out.println(feeTotal);
			
			
			System.out.print("\n\nWould you like to check the cost for another patient?\nType 1 "
					+ "for yes or 0 for no.");
			anotherP = input.nextInt();
			if (anotherP == 1){
				determinate = true;
			} else{
				determinate = false;
				System.out.println("Thank you for using our services, come again!");
			}
			
		}while (determinate)
		
		input.close();
	}

}
