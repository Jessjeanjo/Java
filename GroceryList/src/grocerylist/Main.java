package grocerylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {	
		ArrayList<String> groceryList = new ArrayList<String>(Arrays.asList("Pear","Apple","Spinach","Almond Milk","Yogurt"));
		String newItem; int counter = 0; 
//		boolean determinate;
		
		Scanner in = new Scanner(System.in);
		
//		do {
			System.out.println("Enter a grocery item:");
			newItem = in.nextLine();
		
			for (int i = 0; i < groceryList.size(); i++){
				if (groceryList.get(i) == newItem){
					counter = 1;
				}
			}
		
			if (counter != 1){
				groceryList.add(newItem);
				System.out.println("Your item has been added!\n");
			} else{
				System.out.println("Your item is already on the list. It has not been added.\n");

			}
			
//			System.out.println("Would you like to add another item? Type 1 for yes, 0 for no: ");
//			if (in.nextInt() == 1){
//				determinate = true;
//				counter = 0;
//			} else {
//				determinate = false;
//				System.out.println("Thank you for using our services, come again.");
//				}
//		} while (determinate == true);
		
		in.close();
	}

}
