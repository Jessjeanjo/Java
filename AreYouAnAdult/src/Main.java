
package areyouanadult;

import java.util.Scanner;


public class Main {

	public static void main(String[] args) {
		int age, adultAge = 18;
		Scanner inputStream = new Scanner(System.in);
		
		System.out.println("Give me your age, please");
		age = inputStream.nextInt();
		System.out.print("Thank you! Based off of your age, ");

		System.out.println(age);
		
		if (age >= adultAge){
			System.out.println("I have determined that, you ARE an adult");
		} else {
			System.out.println("I have determined that you are NOT an adult");
		}


		inputStream.close();
}
}
